import Vue from 'vue'
import Vuex from 'vuex'
import chat from '../widgets/chat/store/state'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    chat
  }
})
