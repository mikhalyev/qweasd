import Vue from 'vue'
import VueRouter from 'vue-router'
import Chat from '../widgets/chat'

Vue.use(VueRouter)

const routes = [
  { name: 'chat', path: '/chat/:id', component: Chat }
]
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
