import ChatService from '../../../services/chatApi'
export default {
  namespaced: true,
  state: {
    chatsLoading: false,
    chatMessagesLoading: false,
    messageSending: false,
    currentChatId: null,
    user: {
      id: 1,
      name: 'vasiliy'
    },
    chats: []
  },
  mutations: {
    chatsLoading: (state, isLoading) => {
      state.chatsLoading = isLoading
    },
    chatMessagesLoading: (state, isLoading) => {
      state.chatMessagesLoading = isLoading
    },
    messageSending: (state, isSending) => {
      state.messageSending = isSending
    },
    setChats: (state, chats) => {
      state.chats = chats
    },
    selectChat: (state, chatId) => {
      state.currentChatId = chatId
    },
    setMessages: (state, data) => {
      const chat = state.chats.find(item => item.id === data.id)
      if (chat) {
        chat.parts = data.messages
      }
    },
    addMessage: (state, data) => {
      const chat = state.chats.find(item => item.id === data.id)
      if (chat) {
        chat.parts.push(data.message)
      }
    }
  },
  getters: {
    countMessages: (state) => {
      return state.chats.length
    },
    chatList: (state) => {
      return state.chats
    },
    getChat: state => chatId => {
      let chat = state.chats.find((item) => {
        return item.id === chatId
      })
      return chat === undefined ? null : chat
    },
    chatMessages: (state, getters) => chatId => {
      let chat = getters.getChat(chatId)
      if (chat) {
        return chat.parts
      }
      return null
    }
  },
  actions: {
    loadChats: ({ commit }) => {
      commit('chatsLoading', true)
      return ChatService.getChats().then(result => {
        commit('setChats', result)
        commit('chatsLoading', false)
        return result
      })
    },
    loadChatMessages: ({ commit, getters }, chatId) => {
      let data = { id: chatId, messages: [] }
      commit('setMessages', data)
      commit('chatMessagesLoading', true)
      ChatService.getChatMessages(chatId).then(result => {
        if (result !== null) {
          data.messages = result
          commit('setMessages', data)
          commit('selectChat', chatId)
        }
        commit('chatMessagesLoading', false)
      })
    },
    sendMessage: ({ state, commit }, message) => {
      let msg = {
        'author': state.user.name,
        'created': '2019-08-06 12:38',
        'text': message.text
      }
      let data = {
        id: message.chatId,
        message: msg
      }

      commit('addMessage', data)
    }
  }
}
