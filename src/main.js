import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import moment from 'moment'
import momentVue from 'vue-moment'
import 'moment/locale/ru'

Vue.use(momentVue, { moment })
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
// console.log('Moment locale: !!!! ', lru)
